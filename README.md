
## Scripts habilitados

Para iniciar el proyecto primero asegurate de tener todas las dependencias instaladas. Corre el siguiente comando:

### `yarn install`

Para iniciar el proyecto debes correr el siguiente comando:

### `yarn start`

Para levantar el proyecto debes correr el siguiente comando:

### `yarn dev-server`



Se levantara la aplicación en el puerto 8008 (puerto por defecto en webpack.config.js)
[http://localhost:8008](http://localhost:8008)


================================================================

Algunos temas que debes tener en conocimiento:

- No todos los superheroes tiene imagen asociada. Por ejemplo, Atom III, Black Goliath, Goliath, etc. Este es un problema de la API consumida.

- Puedes obtener un access token desde la siguiente url: https://superheroapi.com/. Solo debes logearte con tu cuenta de FACEBOOK. El token lo reemplaza en el archivo index.js que esta en src/services/axios/index.js (const accessToken). Si no puedes accesar y obtener el token, no hay problema, te dejo el mio ;)

- Puedes cambiar el puerto del proyecto directo desde el archivo webpack.config.js ubicado en la raiz de proyecto



