import React from 'react';
import { Link } from 'react-router-dom';
import Footer from '../../components/FooterComponent';
import Header from '../../components/HeaderComponent';

const NotFound = () => (
    <>
        <Header />
        <h2 color>404 - Página no existe!</h2>
        <Link to="/">
            Volver al HOME
        </Link>
        <Footer />
    </>

);

export default NotFound;