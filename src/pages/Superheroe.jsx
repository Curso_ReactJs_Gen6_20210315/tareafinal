import { Card } from '@material-ui/core';
import { Modal, Spin, Table } from 'antd';
import { Button } from '@material-ui/core';

import React, { useEffect, useState } from 'react'
import Footer from '../components/FooterComponent';
import Header from '../components/HeaderComponent'
import SuperheroeServices from '../services/SuperheroeServices';
import { Link } from 'react-router-dom';
import Util from '../utils/Util'
import store from "store";


const Superheroe = (props) => {

    const idSuperhero = props.match.params.idSuperhero;

    const [foto, setFoto] = useState(null)
    const [nombre, setNombre] = useState("")
    const [visibleSpinner, setVisibleSpinner] = useState(false)
    const [visibleModal, setVisibleModal] = useState(false)

    const [tableData, setTableData] = useState([])
    const tableColumns = [
        {
            title: 'Dato',
            dataIndex: 'dato',
            key: 'dato',
        },
        {
            title: 'Valor',
            dataIndex: 'valor',
            key: 'valor',
        }
    ]


    useEffect(() => {

        setVisibleSpinner(true)
        SuperheroeServices.findSuperheroById(idSuperhero)
        .then(resp => {

            setFoto(resp.data.url)
            setNombre(resp.data.name)
            store.set("nombreSuperheroe", resp.data.name);    
            setVisibleSpinner(false)
        })
        .catch(err => {
            console.log("ERROR", err)
            setVisibleSpinner(false)
        })
    }, [idSuperhero])


    const abrirModal = (idServicio) => {
        setVisibleSpinner(true)
        switch(idServicio) {
 
            case '1':
                SuperheroeServices.findPowerstatsById(idSuperhero)
                .then(resp => {
                    const {combat, durability, intelligence, power, speed, strength} = resp.data

                    let datos = [
                        {
                            "dato": "Combate",
                            "valor": combat
                        },
                        {
                            "dato": "Durabilidad",
                            "valor": durability
                        },
                        {
                            "dato": "Inteligencia",
                            "valor": intelligence
                        },
                        {
                            "dato": "Poder",
                            "valor": power
                        },
                        {
                            "dato": "Velocidad",
                            "valor": speed
                        },
                        {
                            "dato": "Fuerza",
                            "valor": strength
                        },
                    ]

                    setTableData(datos)

                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)

                })
                break;
            
            case '2':
                SuperheroeServices.findBiographyById(idSuperhero)
                .then(resp => {
                    //const {aliases, alignment, alter-egos, first-appearance, full-name, place-of-birth, publisher} = resp.data
                    const {data} = resp

                    let datos = [
                        {
                            "dato": "Alias",
                            "valor": data["aliases"]
                        },
                        {
                            "dato": "Bando",
                            "valor": data["alignment"]
                        },
                        {
                            "dato": "Alter Egos",
                            "valor": data["alter-egos"]
                        },
                        {
                            "dato": "Primera Aparición",
                            "valor": data["first-appearance"]
                        },
                        {
                            "dato": "Nombre Completo",
                            "valor": data["full-name"]
                        },
                        {
                            "dato": "Lugar de Nacimiento",
                            "valor": data["place-of-birth"]
                        },
                        {
                            "dato": "Edición",
                            "valor": data["publisher"]
                        },
                    ]

                    setTableData(datos)
                    
                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                })
                break;       
            case '3':
                SuperheroeServices.findAppearanceById(idSuperhero)
                .then(resp => {
                    const {data} = resp
                    let datos = [
                        {
                            "dato": "Genero",
                            "valor": data["gender"]
                        },
                        {
                            "dato": "Color de Ojos",
                            "valor": data["eye-color"]
                        },
                        {
                            "dato": "Color de Cabello",
                            "valor": data["hair-color"]
                        },
                        {
                            "dato": "Altura",
                            "valor": data["height"].map(item => {return item+" / "})
                        },
                        {
                            "dato": "Peso",
                            "valor": data["weight"].map(item => {return item+" / "})
                        },
                        {
                            "dato": "Raza",
                            "valor": data["race"]
                        }
                    ]

                    setTableData(datos)
                    
                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                }) 
                break;       
      
            case '4':
                SuperheroeServices.findWorkById(idSuperhero)
                .then(resp => {
                    const {data} = resp
                    
                    let datos = [
                        {
                            "dato": "Base",
                            "valor": data["base"]
                        },
                        {
                            "dato": "Empleo",
                            "valor": data["occupation"]
                        },
                    ]

                    setTableData(datos)
                    
                    setVisibleModal(true)
                    setVisibleSpinner(false)
                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                }) 
                break;  

            case '5':
                SuperheroeServices.findConnectionsById(idSuperhero)
                .then(resp => {
                    const {data} = resp
                    
                    let datos = [
                        {
                            "dato": "Grupo(s)",
                            "valor": data["group-affiliation"]
                        },
                        {
                            "dato": "Parientes y amigos",
                            "valor": data["relatives"]
                        },
                    ]

                    setTableData(datos)
                    
                    setVisibleModal(true)
                    setVisibleSpinner(false)

                })
                .catch(err => {
                    console.log(err);
                    setVisibleSpinner(false)
                }) 
                break;  

            default:
                Modal.error({
                    title: (<h2>Opción no encontrada!</h2>),
                    content: (
                        <div>
                            <p>No hemos encontrado esta opción, por favor intentanuevamente</p>
                        </div>
                    ),
                })
            }    
    }


    return (
        <>
            <Spin spinning={visibleSpinner} tip="Cargando ...">
                <Header></Header>
                <br/><br/><br/>
                <Card title={nombre} >
                    <p align="center">
                        <img src={foto} alt={nombre} className="photo"/>
                    </p>
                    <p align="center">
                        {
                            Util.obtenerMenuDetalleSuperheroes().map((item) => {
                                return <Button key={item.id} onClick={(e) => {e.preventDefault(); abrirModal(item.id)}}
                                    color="primary" className='btn-magenta mt-2'> {item.nombre} </Button>
                            })
                        }
                    </p>
                </Card> 
                <p align="center">
                    <Button component={Link} to={`/`} variant="contained"
                        color="primary" className='btn-secondary mt-2'> Volver </Button>
                </p>
            </Spin>
            <Modal 
                visible={visibleModal} 
                confirmLoading={visibleSpinner}
                onOk={() => setVisibleModal(false)}
                closable={false}
                cancelButtonProps={ { style: { display: 'none' } }}
            >
                    <Table columns={tableColumns} dataSource={tableData} pagination={false} />
            </Modal>
            <Footer></Footer>
        </>
    )
}

export default Superheroe;