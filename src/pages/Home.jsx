import React, { useState } from 'react'
import Buscador from '../components/BuscadorComponent';
import Footer from '../components/FooterComponent';
import Header from '../components/HeaderComponent';
import ListaSuperheroes from '../components/ListaSuperheroesComponents';
import store from "store";
const Home = (props) => {

    const [nombreBuscado, setNombreBuscado] = useState(store.get("nombreSuperheroe"));
console.log("nombreSuperheroe", store.get("nombreSuperheroe"));
    return (
        <>
        <Header />
        <Buscador setNombreBuscado={setNombreBuscado} />
        <ListaSuperheroes nombreSuperHeroe={nombreBuscado} />
        <Footer />
        </>
    )
}

export default Home;