import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from 'react-router-dom'
import { Suspense } from 'react';
import { Spin } from 'antd';
import "core-js/stable";
import "regenerator-runtime/runtime";


const loading = <Spin spinning></Spin>

ReactDOM.render(
  <BrowserRouter>
    <Suspense fallback={loading}>
      <App />
    </Suspense>
  </BrowserRouter>,
  document.getElementById('root')
);
