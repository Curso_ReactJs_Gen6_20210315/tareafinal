import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router';
import './scss/styles.scss';
import 'antd/dist/antd.css'; 

const Home = React.lazy(() => import('./pages/Home'));
const NotFound = React.lazy(() => import('./pages/error/404'));
const Superheroe = React.lazy(() => import('./pages/Superheroe'));

const App = () => (
    <Fragment>
        <Switch>
            <Route exact path='/' component={Home}/>
            <Route exact path='/superheroe/:idSuperhero' component={Superheroe}/>
            <Route component={NotFound}/>
        </Switch>
    </Fragment>
  ) ;
  
  export default App;
  