import { Button, Card, Col, Form, Input, Row } from "antd";
import React from 'react'
import PropTypes from 'prop-types';

const Buscador = ({setNombreBuscado = {}}) => {

    const [form] = Form.useForm();

    const onFinish = () => {
        const data = form.getFieldsValue();
        setNombreBuscado(data.nombreSuperheroe);

    }

    return (
        <>
            <Row type="flex" justify="center" className="mt-4" >
                <Col className="col-lg-12 col-md-12 col-xs-12 mb-2" >
                    <Card>
                        <Form form={form}
                            layout="inline"
                            onFinish={onFinish}
                        >
                            <Form.Item name="nombreSuperheroe" label="Nombre del superheroe" className="col-lg-9 col-md-9 col-xs-12 ">
                                <Input placeholder="Ej. Batman" />
                            </Form.Item>

                            <Form.Item className="col-lg-3 col-md-3 col-xs-12 ">
                                <Button type="primary" htmlType="submit" className="btn-search w100 ">
                                    Buscar
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

Buscador.propTypes = {
    setNombreBuscado: PropTypes.func.isRequired,
};

export default Buscador;