
import { Col, Modal, Row, Spin } from 'antd';
import { Content } from 'antd/lib/layout/layout';
import React, { useEffect, useState } from 'react'
import SuperheroeServices from '../services/SuperheroeServices';
import SuperheroeCard from './SuperheroeCardComponents';
import PropTypes from 'prop-types';

const ListaSuperheroes = ({ nombreSuperHeroe = "" }) => {

    const [listado, setListado] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        SuperheroeServices.findSuperheroByName(nombreSuperHeroe)
            .then((resp) => {
                
                setLoading(false);

                let resultado = resp.data
                if (resultado.response === "success")
                    setListado(resultado.results)
                else{
                    setListado([])
                    if(nombreSuperHeroe !== undefined && nombreSuperHeroe !== ''){
                        Modal.warning({
                            title: (<h2>Superheroe no encontrado!</h2>),
                            content: (
                                <div>
                                    <p>No hemos encontrado el superheroe <b>{nombreSuperHeroe}</b>. Por favor intenta con otro superheroe</p>
                                </div>
                            ),
                        })
                    }
                    
                }

                
            }).catch((err) => {
                setLoading(false);
                
                Modal.error({
                    title: (<h2>Houston, tenemos un problema!</h2>),
                    content: (
                        <div>
                            <p>Al parecer la API ha presentado un problema inesperado, intentelo más tarde nuevamente.</p>
                        </div>
                    ),
                })
            })
    }, [nombreSuperHeroe])


    return (
        <>
            <Spin spinning={loading}>
                <Content style={{ padding: '0 50px' }} >
                    <Row gutter={24}>
                    {
                        listado.map(p =>

                            <Col span={8} key={p.id}>
                                <SuperheroeCard id={p.id} nombre={p.name} img={p.image.url}></SuperheroeCard>
                            </Col>

                        )
                    }
                    </Row>
                </Content >
            </Spin>    
        </>
    )
}

ListaSuperheroes.propTypes = {
    nombreSuperHeroe: PropTypes.string.isRequired,
};

export default ListaSuperheroes;
