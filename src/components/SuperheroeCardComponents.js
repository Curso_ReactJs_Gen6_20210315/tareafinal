import { Button } from '@material-ui/core';
import { Card } from 'antd';
import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const SuperheroeCard = ({id, nombre, img} = {}) => {
    return (
        <Fragment key={id}>
            
            <Card title={nombre} >
                <p align="center">
                    <img src={img} alt={nombre} className="photo"/>
                </p>
                <p align="center">
                    <Button component={Link} to={`/superheroe/${id}`} variant="contained"
                            color="primary" className='btn-magenta mt-2'> Ver Detalles </Button>
                </p>
            </Card> 
        </Fragment>
    )
}

SuperheroeCard.propTypes = {
    id: PropTypes.string.isRequired,
    nombre: PropTypes.string.isRequired,
    img: PropTypes.string.isRequired,
    
};

export default SuperheroeCard;
