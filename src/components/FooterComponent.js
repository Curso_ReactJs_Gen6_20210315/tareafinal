import React from 'react'

const Footer = () =>  (
    <footer>
        <p className="mt-1">&copy; {new Date().getFullYear()} - Felipe Monti</p>
    </footer>
)

export default Footer;
