const path = require("path");

module.exports = {
    //mode: "development",
    mode: process.env.NODE_ENV === "production" ? "production" : "development",
    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "public"),
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.svg$/,
                use: ["@scgr/webpack"],
            },
            {
                test: /\.(gif|png|jpeg|ico)$/i,
                use: [
                    "file-loader",
                    {
                        loader: "image-webpack-loader",
                        options: {
                            bypassOnDebug: true,
                            disable: true,
                        },
                    },
                ],
            },
            {
                test: /\.s[ac]ss$/i,
                use: ["style-loader","css-loader","sass-loader",]
            },
        ],
    },
    resolve: {
        extensions: [".js", ".jsx"],
    },
    performance: {
        hints: process.env.NODE_ENV === "production" ? "error": false,
        maxEntrypointSize: 500000,
        maxAssetSize: 500000,
    },
    //devtool: process.env.NODE_ENV === "production" ? "source-map" : "eval-ceap-module-source-map",
    devtool: "source-map",
    devServer: {
        contentBase: path.join(__dirname, "public"),
        port:8008
    },
};