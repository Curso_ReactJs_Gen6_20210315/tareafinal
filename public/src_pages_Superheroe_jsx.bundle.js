(self["webpackChunktarea1_app"] = self["webpackChunktarea1_app"] || []).push([["src_pages_Superheroe_jsx"],{

/***/ "./src/components/FooterComponent.js":
/*!*******************************************!*\
  !*** ./src/components/FooterComponent.js ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");


var Footer = function Footer() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("footer", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    className: "mt-1"
  }, "\xA9 ", new Date().getFullYear(), " - Felipe Monti"));
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./src/components/HeaderComponent.js":
/*!*******************************************!*\
  !*** ./src/components/HeaderComponent.js ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/AppBar/AppBar.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/Toolbar/Toolbar.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");



var Header = function Header() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_1__.default, {
    position: "sticky"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_2__.default, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", null, "SUPERHEROES APP")));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./src/pages/Superheroe.jsx":
/*!**********************************!*\
  !*** ./src/pages/Superheroe.jsx ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/Card/Card.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/modal/index.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/spin/index.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/table/index.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/Button/Button.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _components_FooterComponent__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/FooterComponent */ "./src/components/FooterComponent.js");
/* harmony import */ var _components_HeaderComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/HeaderComponent */ "./src/components/HeaderComponent.js");
/* harmony import */ var _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/SuperheroeServices */ "./src/services/SuperheroeServices.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _utils_Util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/Util */ "./src/utils/Util.js");
/* harmony import */ var store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! store */ "./node_modules/store/dist/store.legacy.js");
/* harmony import */ var store__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(store__WEBPACK_IMPORTED_MODULE_5__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }












var Superheroe = function Superheroe(props) {
  var idSuperhero = props.match.params.idSuperhero;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      foto = _useState2[0],
      setFoto = _useState2[1];

  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(""),
      _useState4 = _slicedToArray(_useState3, 2),
      nombre = _useState4[0],
      setNombre = _useState4[1];

  var _useState5 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      visibleSpinner = _useState6[0],
      setVisibleSpinner = _useState6[1];

  var _useState7 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      visibleModal = _useState8[0],
      setVisibleModal = _useState8[1];

  var _useState9 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)([]),
      _useState10 = _slicedToArray(_useState9, 2),
      tableData = _useState10[0],
      setTableData = _useState10[1];

  var tableColumns = [{
    title: 'Dato',
    dataIndex: 'dato',
    key: 'dato'
  }, {
    title: 'Valor',
    dataIndex: 'valor',
    key: 'valor'
  }];
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    setVisibleSpinner(true);
    _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__.default.findSuperheroById(idSuperhero).then(function (resp) {
      setFoto(resp.data.url);
      setNombre(resp.data.name);
      store__WEBPACK_IMPORTED_MODULE_5___default().set("nombreSuperheroe", resp.data.name);
      setVisibleSpinner(false);
    }).catch(function (err) {
      console.log("ERROR", err);
      setVisibleSpinner(false);
    });
  }, [idSuperhero]);

  var abrirModal = function abrirModal(idServicio) {
    setVisibleSpinner(true);

    switch (idServicio) {
      case '1':
        _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__.default.findPowerstatsById(idSuperhero).then(function (resp) {
          var _resp$data = resp.data,
              combat = _resp$data.combat,
              durability = _resp$data.durability,
              intelligence = _resp$data.intelligence,
              power = _resp$data.power,
              speed = _resp$data.speed,
              strength = _resp$data.strength;
          var datos = [{
            "dato": "Combate",
            "valor": combat
          }, {
            "dato": "Durabilidad",
            "valor": durability
          }, {
            "dato": "Inteligencia",
            "valor": intelligence
          }, {
            "dato": "Poder",
            "valor": power
          }, {
            "dato": "Velocidad",
            "valor": speed
          }, {
            "dato": "Fuerza",
            "valor": strength
          }];
          setTableData(datos);
          setVisibleModal(true);
          setVisibleSpinner(false);
        }).catch(function (err) {
          console.log(err);
          setVisibleSpinner(false);
        });
        break;

      case '2':
        _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__.default.findBiographyById(idSuperhero).then(function (resp) {
          //const {aliases, alignment, alter-egos, first-appearance, full-name, place-of-birth, publisher} = resp.data
          var data = resp.data;
          var datos = [{
            "dato": "Alias",
            "valor": data["aliases"]
          }, {
            "dato": "Bando",
            "valor": data["alignment"]
          }, {
            "dato": "Alter Egos",
            "valor": data["alter-egos"]
          }, {
            "dato": "Primera Aparición",
            "valor": data["first-appearance"]
          }, {
            "dato": "Nombre Completo",
            "valor": data["full-name"]
          }, {
            "dato": "Lugar de Nacimiento",
            "valor": data["place-of-birth"]
          }, {
            "dato": "Edición",
            "valor": data["publisher"]
          }];
          setTableData(datos);
          setVisibleModal(true);
          setVisibleSpinner(false);
        }).catch(function (err) {
          console.log(err);
          setVisibleSpinner(false);
        });
        break;

      case '3':
        _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__.default.findAppearanceById(idSuperhero).then(function (resp) {
          var data = resp.data;
          var datos = [{
            "dato": "Genero",
            "valor": data["gender"]
          }, {
            "dato": "Color de Ojos",
            "valor": data["eye-color"]
          }, {
            "dato": "Color de Cabello",
            "valor": data["hair-color"]
          }, {
            "dato": "Altura",
            "valor": data["height"].map(function (item) {
              return item + " / ";
            })
          }, {
            "dato": "Peso",
            "valor": data["weight"].map(function (item) {
              return item + " / ";
            })
          }, {
            "dato": "Raza",
            "valor": data["race"]
          }];
          setTableData(datos);
          setVisibleModal(true);
          setVisibleSpinner(false);
        }).catch(function (err) {
          console.log(err);
          setVisibleSpinner(false);
        });
        break;

      case '4':
        _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__.default.findWorkById(idSuperhero).then(function (resp) {
          var data = resp.data;
          var datos = [{
            "dato": "Base",
            "valor": data["base"]
          }, {
            "dato": "Empleo",
            "valor": data["occupation"]
          }];
          setTableData(datos);
          setVisibleModal(true);
          setVisibleSpinner(false);
        }).catch(function (err) {
          console.log(err);
          setVisibleSpinner(false);
        });
        break;

      case '5':
        _services_SuperheroeServices__WEBPACK_IMPORTED_MODULE_3__.default.findConnectionsById(idSuperhero).then(function (resp) {
          var data = resp.data;
          var datos = [{
            "dato": "Grupo(s)",
            "valor": data["group-affiliation"]
          }, {
            "dato": "Parientes y amigos",
            "valor": data["relatives"]
          }];
          setTableData(datos);
          setVisibleModal(true);
          setVisibleSpinner(false);
        }).catch(function (err) {
          console.log(err);
          setVisibleSpinner(false);
        });
        break;

      default:
        antd__WEBPACK_IMPORTED_MODULE_6__.default.error({
          title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h2", null, "Opci\xF3n no encontrada!"),
          content: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null, "No hemos encontrado esta opci\xF3n, por favor intentanuevamente"))
        });
    }
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(antd__WEBPACK_IMPORTED_MODULE_7__.default, {
    spinning: visibleSpinner,
    tip: "Cargando ..."
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_HeaderComponent__WEBPACK_IMPORTED_MODULE_2__.default, null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_8__.default, {
    title: nombre
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    align: "center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: foto,
    alt: nombre,
    className: "photo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    align: "center"
  }, _utils_Util__WEBPACK_IMPORTED_MODULE_4__.default.obtenerMenuDetalleSuperheroes().map(function (item) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__.default, {
      key: item.id,
      onClick: function onClick(e) {
        e.preventDefault();
        abrirModal(item.id);
      },
      color: "primary",
      className: "btn-magenta mt-2"
    }, " ", item.nombre, " ");
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    align: "center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_material_ui_core__WEBPACK_IMPORTED_MODULE_9__.default, {
    component: react_router_dom__WEBPACK_IMPORTED_MODULE_10__.Link,
    to: "/",
    variant: "contained",
    color: "primary",
    className: "btn-secondary mt-2"
  }, " Volver "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(antd__WEBPACK_IMPORTED_MODULE_6__.default, {
    visible: visibleModal,
    confirmLoading: visibleSpinner,
    onOk: function onOk() {
      return setVisibleModal(false);
    },
    closable: false,
    cancelButtonProps: {
      style: {
        display: 'none'
      }
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(antd__WEBPACK_IMPORTED_MODULE_11__.default, {
    columns: tableColumns,
    dataSource: tableData,
    pagination: false
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_FooterComponent__WEBPACK_IMPORTED_MODULE_1__.default, null));
};

/* harmony default export */ __webpack_exports__["default"] = (Superheroe);

/***/ }),

/***/ "./src/services/SuperheroeServices.js":
/*!********************************************!*\
  !*** ./src/services/SuperheroeServices.js ***!
  \********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/axios */ "./src/services/axios/index.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var findSuperheroById = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(id) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("".concat(id, "/image"));

          case 3:
            return _context.abrupt("return", _context.sent);

          case 6:
            _context.prev = 6;
            _context.t0 = _context["catch"](0);
            throw new Error('Error al traer los superheroes.');

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 6]]);
  }));

  return function findSuperheroById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var findBiographyById = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(id) {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("".concat(id, "/biography"));

          case 3:
            return _context2.abrupt("return", _context2.sent);

          case 6:
            _context2.prev = 6;
            _context2.t0 = _context2["catch"](0);
            throw new Error('Error al traer la biografia.');

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 6]]);
  }));

  return function findBiographyById(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

var findAppearanceById = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(id) {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("".concat(id, "/appearance"));

          case 3:
            return _context3.abrupt("return", _context3.sent);

          case 6:
            _context3.prev = 6;
            _context3.t0 = _context3["catch"](0);
            throw new Error('Error al traer la apariencia.');

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 6]]);
  }));

  return function findAppearanceById(_x3) {
    return _ref3.apply(this, arguments);
  };
}();

var findWorkById = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(id) {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("".concat(id, "/work"));

          case 3:
            return _context4.abrupt("return", _context4.sent);

          case 6:
            _context4.prev = 6;
            _context4.t0 = _context4["catch"](0);
            throw new Error('Error al traer el trabajo.');

          case 9:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 6]]);
  }));

  return function findWorkById(_x4) {
    return _ref4.apply(this, arguments);
  };
}();

var findPowerstatsById = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(id) {
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.prev = 0;
            _context5.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("".concat(id, "/powerstats"));

          case 3:
            return _context5.abrupt("return", _context5.sent);

          case 6:
            _context5.prev = 6;
            _context5.t0 = _context5["catch"](0);
            throw new Error('Error al traer nivel de poder.');

          case 9:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[0, 6]]);
  }));

  return function findPowerstatsById(_x5) {
    return _ref5.apply(this, arguments);
  };
}();

var findConnectionsById = /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(id) {
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            _context6.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("".concat(id, "/connections"));

          case 3:
            return _context6.abrupt("return", _context6.sent);

          case 6:
            _context6.prev = 6;
            _context6.t0 = _context6["catch"](0);
            throw new Error('Error al traer nivel de poder.');

          case 9:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 6]]);
  }));

  return function findConnectionsById(_x6) {
    return _ref6.apply(this, arguments);
  };
}();

var findSuperheroByName = /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(name) {
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.prev = 0;
            _context7.next = 3;
            return _services_axios__WEBPACK_IMPORTED_MODULE_0__.default.get("search/".concat(name));

          case 3:
            return _context7.abrupt("return", _context7.sent);

          case 6:
            _context7.prev = 6;
            _context7.t0 = _context7["catch"](0);
            throw new Error("Error al traer los superheroes por nombre.".concat(_context7.t0));

          case 9:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7, null, [[0, 6]]);
  }));

  return function findSuperheroByName(_x7) {
    return _ref7.apply(this, arguments);
  };
}();
/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */


/* harmony default export */ __webpack_exports__["default"] = ({
  findSuperheroById: findSuperheroById,
  findBiographyById: findBiographyById,
  findAppearanceById: findAppearanceById,
  findWorkById: findWorkById,
  findPowerstatsById: findPowerstatsById,
  findConnectionsById: findConnectionsById,
  findSuperheroByName: findSuperheroByName
});

/***/ }),

/***/ "./src/services/axios/index.js":
/*!*************************************!*\
  !*** ./src/services/axios/index.js ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

var url_base = "https://superheroapi.com/api.php/";
var accessToken = "10159021108151002";
var apiClient = axios__WEBPACK_IMPORTED_MODULE_0___default().create({
  baseURL: url_base + accessToken + '/'
});
/* harmony default export */ __webpack_exports__["default"] = (apiClient);

/***/ }),

/***/ "./src/utils/Util.js":
/*!***************************!*\
  !*** ./src/utils/Util.js ***!
  \***************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var maximo = 730;
var minimo = 0;
var nombresSuperHeroes = ["A-Bomb", "Abe Sapien", "Abin Sur", "Abomination", "Abraxas", "Absorbing Man", "Adam Monroe", "Adam Strange", "Agent 13", "Agent Bob", "Agent Zero", "Air-Walker", "Ajax", "Alan Scott", "Alex Mercer", "Alex Woolsly", "Alfred Pennyworth", "Alien", "Allan Quatermain", "Amazo", "Ammo", "Ando Masahashi", "Angel", "Angel", "Angel Dust", "Angel Salvadore", "Angela", "Animal Man", "Annihilus", "Ant-Man", "Ant-Man II", "Anti-Monitor", "Anti-Spawn", "Anti-Venom", "Apocalypse", "Aquababy", "Aqualad", "Aquaman", "Arachne", "Archangel", "Arclight", "Ardina", "Ares", "Ariel", "Armor", "Arsenal", "Astro Boy", "Atlas", "Atlas", "Atom", "Atom", "Atom Girl", "Atom II", "Atom III", "Atom IV", "Aurora", "Azazel", "Azrael", "Aztar", "Bane", "Banshee", "Bantam", "Batgirl", "Batgirl", "Batgirl III", "Batgirl IV", "Batgirl V", "Batgirl VI", "Batman", "Batman", "Batman II", "Battlestar", "Batwoman V", "Beak", "Beast", "Beast Boy", "Beetle", "Ben 10", "Beta Ray Bill", "Beyonder", "Big Barda", "Big Daddy", "Big Man", "Bill Harken", "Billy Kincaid", "Binary", "Bionic Woman", "Bird-Brain", "Bird-Man", "Bird-Man II", "Birdman", "Bishop", "Bizarro", "Black Abbott", "Black Adam", "Black Bolt", "Black Canary", "Black Canary", "Black Cat", "Black Flash", "Black Goliath", "Black Knight III", "Black Lightning", "Black Mamba", "Black Manta", "Black Panther", "Black Widow", "Black Widow II", "Blackout", "Blackwing", "Blackwulf", "Blade", "Blaquesmith", "Bling!", "Blink", "Blizzard", "Blizzard", "Blizzard II", "Blob", "Bloodaxe", "Bloodhawk", "Bloodwraith", "Blue Beetle", "Blue Beetle", "Blue Beetle II", "Blue Beetle III", "Boba Fett", "Bolt", "Bomb Queen", "Boom-Boom", "Boomer", "Booster Gold", "Box", "Box III", "Box IV", "Brainiac", "Brainiac 5", "Brother Voodoo", "Brundlefly", "Buffy", "Bullseye", "Bumblebee", "Bumbleboy", "Bushido", "Cable", "Callisto", "Cameron Hicks", "Cannonball", "Captain America", "Captain Atom", "Captain Britain", "Captain Cold", "Captain Epic", "Captain Hindsight", "Captain Mar-vell", "Captain Marvel", "Captain Marvel", "Captain Marvel II", "Captain Midnight", "Captain Planet", "Captain Universe", "Carnage", "Cat", "Cat II", "Catwoman", "Cecilia Reyes", "Century", "Cerebra", "Chamber", "Chameleon", "Changeling", "Cheetah", "Cheetah II", "Cheetah III", "Chromos", "Chuck Norris", "Citizen Steel", "Claire Bennet", "Clea", "Cloak", "Clock King", "Cogliostro", "Colin Wagner", "Colossal Boy", "Colossus", "Copycat", "Corsair", "Cottonmouth", "Crimson Crusader", "Crimson Dynamo", "Crystal", "Curse", "Cy-Gor", "Cyborg", "Cyborg Superman", "Cyclops", "Cypher", "Dagger", "Danny Cooper", "Daphne Powell", "Daredevil", "Darkhawk", "Darkman", "Darkseid", "Darkside", "Darkstar", "Darth Maul", "Darth Vader", "Dash", "Data", "Dazzler", "Deadman", "Deadpool", "Deadshot", "Deathlok", "Deathstroke", "Demogoblin", "Destroyer", "Diamondback", "DL Hawkins", "Doc Samson", "Doctor Doom", "Doctor Doom II", "Doctor Fate", "Doctor Octopus", "Doctor Strange", "Domino", "Donatello", "Donna Troy", "Doomsday", "Doppelganger", "Dormammu", "Dr Manhattan", "Drax the Destroyer", "Ego", "Elastigirl", "Electro", "Elektra", "Elle Bishop", "Emma Frost", "Enchantress", "Energy", "ERG-1", "Ethan Hunt", "Etrigan", "Evil Deadpool", "Evilhawk", "Exodus", "Fabian Cortez", "Falcon", "Fallen One II", "Faora", "Feral", "Fighting Spirit", "Fin Fang Foom", "Firebird", "Firelord", "Firestar", "Firestorm", "Firestorm", "Fixer", "Flash", "Flash Gordon", "Flash II", "Flash III", "Flash IV", "Forge", "Franklin Richards", "Franklin Storm", "Frenzy", "Frigga", "Galactus", "Gambit", "Gamora", "Garbage Man", "Gary Bell", "General Zod", "Genesis", "Ghost Rider", "Ghost Rider II", "Giant-Man", "Giant-Man II", "Giganta", "Gladiator", "Goblin Queen", "Godzilla", "Gog", "Goku", "Goliath", "Goliath", "Goliath", "Goliath IV", "Gorilla Grodd", "Granny Goodness", "Gravity", "Greedo", "Green Arrow", "Green Goblin", "Green Goblin II", "Green Goblin III", "Green Goblin IV", "Groot", "Guardian", "Guy Gardner", "Hal Jordan", "Han Solo", "Hancock", "Harley Quinn", "Harry Potter", "Havok", "Hawk", "Hawkeye", "Hawkeye II", "Hawkgirl", "Hawkman", "Hawkwoman", "Hawkwoman II", "Hawkwoman III", "Heat Wave", "Hela", "Hellboy", "Hellcat", "Hellstorm", "Hercules", "Hiro Nakamura", "Hit-Girl", "Hobgoblin", "Hollow", "Hope Summers", "Howard the Duck", "Hulk", "Human Torch", "Huntress", "Husk", "Hybrid", "Hydro-Man", "Hyperion", "Iceman", "Impulse", "Indiana Jones", "Indigo", "Ink", "Invisible Woman", "Iron Fist", "Iron Man", "Iron Monger", "Isis", "Jack Bauer", "Jack of Hearts", "Jack-Jack", "James Bond", "James T. Kirk", "Jar Jar Binks", "Jason Bourne", "Jean Grey", "Jean-Luc Picard", "Jennifer Kale", "Jesse Quick", "Jessica Cruz", "Jessica Jones", "Jessica Sanders", "Jigsaw", "Jim Powell", "JJ Powell", "Johann Krauss", "John Constantine", "John Stewart", "John Wraith", "Joker", "Jolt", "Jubilee", "Judge Dredd", "Juggernaut", "Junkpile", "Justice", "Jyn Erso", "K-2SO", "Kang", "Kathryn Janeway", "Katniss Everdeen", "Kevin 11", "Kick-Ass", "Kid Flash", "Kid Flash II", "Killer Croc", "Killer Frost", "Kilowog", "King Kong", "King Shark", "Kingpin", "Klaw", "Kool-Aid Man", "Kraven II", "Kraven the Hunter", "Krypto", "Kyle Rayner", "Kylo Ren", "Lady Bullseye", "Lady Deathstrike", "Leader", "Leech", "Legion", "Leonardo", "Lex Luthor", "Light Lass", "Lightning Lad", "Lightning Lord", "Living Brain", "Living Tribunal", "Liz Sherman", "Lizard", "Lobo", "Loki", "Longshot", "Luke Cage", "Luke Campbell", "Luke Skywalker", "Luna", "Lyja", "Mach-IV", "Machine Man", "Magneto", "Magog", "Magus", "Man of Miracles", "Man-Bat", "Man-Thing", "Man-Wolf", "Mandarin", "Mantis", "Martian Manhunter", "Marvel Girl", "Master Brood", "Master Chief", "Match", "Matt Parkman", "Maverick", "Maxima", "Maya Herrera", "Medusa", "Meltdown", "Mephisto", "Mera", "Metallo", "Metamorpho", "Meteorite", "Metron", "Micah Sanders", "Michelangelo", "Micro Lad", "Mimic", "Minna Murray", "Misfit", "Miss Martian", "Mister Fantastic", "Mister Freeze", "Mister Knife", "Mister Mxyzptlk", "Mister Sinister", "Mister Zsasz", "Mockingbird", "MODOK", "Mogo", "Mohinder Suresh", "Moloch", "Molten Man", "Monarch", "Monica Dawson", "Moon Knight", "Moonstone", "Morlun", "Morph", "Moses Magnum", "Mr Immortal", "Mr Incredible", "Ms Marvel II", "Multiple Man", "Mysterio", "Mystique", "Namor", "Namor", "Namora", "Namorita", "Naruto Uzumaki", "Nathan Petrelli", "Nebula", "Negasonic Teenage Warhead", "Nick Fury", "Nightcrawler", "Nightwing", "Niki Sanders", "Nina Theroux", "Nite Owl II", "Northstar", "Nova", "Nova", "Odin", "Offspring", "Omega Red", "Omniscient", "One Punch Man", "One-Above-All", "Onslaught", "Oracle", "Osiris", "Overtkill", "Ozymandias", "Parademon", "Paul Blart", "Penance", "Penance I", "Penance II", "Penguin", "Phantom", "Phantom Girl", "Phoenix", "Plantman", "Plastic Lad", "Plastic Man", "Plastique", "Poison Ivy", "Polaris", "Power Girl", "Power Man", "Predator", "Professor X", "Professor Zoom", "Psylocke", "Punisher", "Purple Man", "Pyro", "Q", "Quantum", "Question", "Quicksilver", "Quill", "Ra's Al Ghul", "Rachel Pirzad", "Rambo", "Raphael", "Raven", "Ray", "Razor-Fist II", "Red Arrow", "Red Hood", "Red Hulk", "Red Mist", "Red Robin", "Red Skull", "Red Tornado", "Redeemer II", "Redeemer III", "Renata Soliz", "Rey", "Rhino", "Rick Flag", "Riddler", "Rip Hunter", "Ripcord", "Robin", "Robin II", "Robin III", "Robin V", "Robin VI", "Rocket Raccoon", "Rogue", "Ronin", "Rorschach", "Sabretooth", "Sage", "Sandman", "Sasquatch", "Sauron", "Savage Dragon", "Scarecrow", "Scarlet Spider", "Scarlet Spider II", "Scarlet Witch", "Scorpia", "Scorpion", "Sebastian Shaw", "Sentry", "Shadow King", "Shadow Lass", "Shadowcat", "Shang-Chi", "Shatterstar", "She-Hulk", "She-Thing", "Shocker", "Shriek", "Shrinking Violet", "Sif", "Silk", "Silk Spectre", "Silk Spectre II", "Silver Surfer", "Silverclaw", "Simon Baz", "Sinestro", "Siren", "Siren II", "Siryn", "Skaar", "Snake-Eyes", "Snowbird", "Sobek", "Solomon Grundy", "Songbird", "Space Ghost", "Spawn", "Spectre", "Speedball", "Speedy", "Speedy", "Spider-Carnage", "Spider-Girl", "Spider-Gwen", "Spider-Man", "Spider-Man", "Spider-Man", "Spider-Woman", "Spider-Woman II", "Spider-Woman III", "Spider-Woman IV", "Spock", "Spyke", "Stacy X", "Star-Lord", "Stardust", "Starfire", "Stargirl", "Static", "Steel", "Stephanie Powell", "Steppenwolf", "Storm", "Stormtrooper", "Sunspot", "Superboy", "Superboy-Prime", "Supergirl", "Superman", "Swamp Thing", "Swarm", "Sylar", "Synch", "T-1000", "T-800", "T-850", "T-X", "Taskmaster", "Tempest", "Thanos", "The Cape", "The Comedian", "Thing", "Thor", "Thor Girl", "Thunderbird", "Thunderbird II", "Thunderbird III", "Thunderstrike", "Thundra", "Tiger Shark", "Tigra", "Tinkerer", "Titan", "Toad", "Toxin", "Toxin", "Tracy Strauss", "Trickster", "Trigon", "Triplicate Girl", "Triton", "Two-Face", "Ultragirl", "Ultron", "Utgard-Loki", "Vagabond", "Valerie Hart", "Valkyrie", "Vanisher", "Vegeta", "Venom", "Venom II", "Venom III", "Venompool", "Vertigo II", "Vibe", "Vindicator", "Vindicator", "Violator", "Violet Parr", "Vision", "Vision II", "Vixen", "Vulcan", "Vulture", "Walrus", "War Machine", "Warbird", "Warlock", "Warp", "Warpath", "Wasp", "Watcher", "Weapon XI", "White Canary", "White Queen", "Wildfire", "Winter Soldier", "Wiz Kid", "Wolfsbane", "Wolverine", "Wonder Girl", "Wonder Man", "Wonder Woman", "Wondra", "Wyatt Wingfoot", "X-23", "X-Man", "Yellow Claw", "Yellowjacket", "Yellowjacket II", "Ymir", "Yoda", "Zatanna", "Zoom"];
var menuDetalleSuperheroe = [{
  "id": "1",
  "nombre": "PowerStats"
}, {
  "id": "2",
  "nombre": "Biography"
}, {
  "id": "3",
  "nombre": "Appearance"
}, {
  "id": "4",
  "nombre": "Work"
}, {
  "id": "5",
  "nombre": "Connections"
}];

var obtenerNombreAleatorio = function obtenerNombreAleatorio() {
  var pos = Math.round(Math.random() * (maximo - minimo) + minimo);
  return nombresSuperHeroes[pos];
};

var obtenerMenuDetalleSuperheroes = function obtenerMenuDetalleSuperheroes() {
  return menuDetalleSuperheroe;
};
/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */


/* harmony default export */ __webpack_exports__["default"] = ({
  obtenerNombreAleatorio: obtenerNombreAleatorio,
  obtenerMenuDetalleSuperheroes: obtenerMenuDetalleSuperheroes
});

/***/ })

}]);
//# sourceMappingURL=src_pages_Superheroe_jsx.bundle.js.map